# method(+)
@which 3.0 + 3.0
@which 3.0 + 3
@which 3.0 + 3.0

#extend + operator:
import Base: +
@which "hello " + "world!"

+(x::String, y::String) = string(x, y)
"hello " + "world!"
@which "hello " + "world!"

foo(x, y) = print("duck-typed foo!")
foo(x::Int, y::Float64) = print("int-float foo!")
foo(x::Int, y::Float64) = print("foo with two floats")
foo(x::Int, y::Float64) = print("foo with two integers")

foo(1, 1)
foo(1., 1.)
foo(1, 1.)
foo(true, false)

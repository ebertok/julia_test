using Plots

N = 11111
t = 1
#v(x) = (rand(N))[x] #random potential
v(x) = (rand(N))[x] #random potential
δ = 0
w = 0

diagonal = [w*(-1)^i+v(i) for i in 1:N]
offdiagonal = [-t*(1 - δ * (-1)^i) for i in 1:N-1]
A = SymTridiagonal(diagonal, offdiagonal)

#=
A = zeros(N, N)
for i in range(1, N - 1)
    A[i,i + 1] = -t*(1 - δ * (-1)^i)
    A[i + 1,i] = -t*(1 - δ * (-1)^i)
    A[i,i] = τ*(-1)^i + v(i)
end
A[N,N] = τ*(-1)^N + v(N)
A
=#


Λ = eigvecs(A)
λ = eigvals(A)

gr()

scatter(λ, label="points")
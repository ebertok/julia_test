function sayhi(name)
    println("Hi $name !")
end

sayhi("C-3PO")

#alternatively:

sayhi2(name) = println("Hi $name !")

sayhi2("Luke")

#anonymous /lambda functions:

sayhi3 = name -> println("Hi $name !")
f = x -> x^2

# Duck-typing, if it quacks like a duck its a duck!
sayhi(553425235)
A = rand(3, 3)
display(A)
f(A)

### Mutating vs non mutating functions
# !: mutating

v = [3,5,2]
sort(v)
v
sort!(v)
v

#broadcasting: non br vs br.
#f() vs f.()
#f() treats object as whole. f.() dissects it

A = [i + 3 * j for j in 0:2, i in 1:3]

f(A) #matrix square
f.(A) #square of every entry
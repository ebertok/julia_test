#benchmark sum function:
#Pkg.add("BenchmarkTools") (once)
#in milliseconds: / 1e6
#use numpy functions with "using Conda"
using BenchmarkTools

a = rand(10^7)
sum(a)

b=rand(10^7)
sum(b)

sum(a)≈sum(b) #≈ : \approx <TAB>

?isapprox ## looks up function implementation

Ψ = rand(100)

for x ∈ rand(6)
    println(x)
end

#benchmark sum:

jbench = @benchmark sum($a)


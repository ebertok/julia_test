A = rand(1:4, 3, 3)

B = A
C = copy(A)
[B C]

A[1]=17
[B C] # C untouched !
##############################

x = ones(3)

b=A*x #mult

sym = A + A' # 'conj trans, .' trans.

Apd = A'A # mult.

#solving linear system :
A\b #Ax=b solve for x

#######overdet:
Atall = A[:,1:2] #keep all rows and first 2 colums of A

Atall\b #calcs least squares sol.

#singular matrix: outer product of vector with itself:
[A[:,1] A[:,1]]\b #minimum norm leasts squares

#######underdet:
Ashort = A[1:2,:]
Ashort\b[1:2] #minimum norm solution


println("Hello World!")

my_answer = 42
typeof(my_answer)
#=
BLAHBLAHBLAHBLAHBLAHBLAH
BLAHBLAHBLAH
BLAHBLAHBLAH
=#
###string :
name = "Eric"
num_fingers = 10
num_toes = 10

output = "Hello, my name is $name, I have $num_fingers fingers and $num_toes toes. That is $(num_fingers + num_toes) in total!"
println(output)

#concatenation:
string("hi ", 10, "lol")

###data structures:

#dictionary: (not ordered)
phonebook = Dict("Eric" => "05609-7214", "Peter" => "999-9999")
phonebook["Julia"] = "555-FILK"
phonebook

phonebook["Eric"]

#pop function: deletes and outputs entry:
pop!(phonebook, "Julia")
phonebook


#Tuples: (immutable)
tupleex = ("hello", "this", "is", "ordered")
tupleex[1]
tupleex[2]

#arrays: (mutable)
myfriends = ["Ted", 2.0, 5]

#add items: push! \ pop! last element
push!(myfriends, "pi")
myfriends
pop!(myfriends)
myfriends

#multidim:
numbers =[[1,2,3],[4,5]]
numbers
random= rand(4,3,2)



x = 4
y = 2523

if x > y
    println("x>y")
elseif y > x
    println("y>x")
else
    println("x=y")
end

if x > y
    x
else
    y
end

#ternary op:

(x > y) ? x : y

#short circuting:

(x > y) && println("x>y")
(y > x) && println("y>x")

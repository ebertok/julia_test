n = 0
while n < 10
    n += 1
    println(n)
end

myfriends = ["Ted", "Robyn", "Alex"]

n = 1
while n <= length(myfriends)
    friend = myfriends[n]
    println("Hi $friend, it's great to see you!")
    n += 1
end

for i in 1:10
    println(i)
end

#addition tables:
m, n = 5, 5
A = zeros(m, n)
for i in 1:m
    for j in 1:n
        A[i,j] = i + j
    end
end
A

B = zeros(m, n)
for i in 1:m, j in 1:n
    B[i,j] = i + j
end

#array comprehension:

C = [i + j for i in 1:m, j in 1:n]

for n in 1:10
    A = [i+j for i in 1:n, j in 1:n]
    display(A)
end
